package cage;
import animal.*;
import java.util.*;
public class Cage{

    private Animal animal;

    public Cage(Animal animal){
        this.animal = animal;
    }

    public Animal getAnimal(){
        return animal;
    }

    public String getType(){
        if(animal.getType().equals("Lion") || animal.getType().equals("Eagle")){
            return "outdoor";
        } else {
            return "indoor";
        }
    }

    public String getSize(){
        if(getType().equals("indoor")){
            if(animal.getBodyLength() < 45){
                return "A";
            }else if(animal.getBodyLength() <= 60){
                return "B";
            }else{
                return "C";
            }
        }else{
            if(animal.getBodyLength() < 75){
                return "A";
            }else if(animal.getBodyLength() <= 90){
                return "B";
            }else{
                return "C";
            }

        }
    }

    public String toString(){
        return animal.getName() + " (" + animal.getBodyLength() + " - " + getSize() + ")";
    }
}