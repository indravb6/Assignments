package cage;
import animal.*;
import java.util.*;
public class Arrangement{
    private ArrayList<Cage> arrangedCage[] = new ArrayList[3]; 
    private int cageCount;
    private ArrayList<Cage> cageList;

    public Arrangement(){

    }

    public Arrangement(ArrayList<Cage> cageList){
        this.cageList = cageList;
        cageCount = cageList.size();
        arrangedCage[0] = new ArrayList<Cage>();
        arrangedCage[1] = new ArrayList<Cage>();
        arrangedCage[2] = new ArrayList<Cage>();
    }

    public void arrange(){
        if(cageCount == 1){
            arrangedCage[0].add(cageList.get(0));
        }else if(cageCount == 2){
            arrangedCage[0].add(cageList.get(0));
            arrangedCage[1].add(cageList.get(1));
        }else{
            for(int i = 0; i < cageCount; i++){
                if(i < cageCount / 3){
                    arrangedCage[0].add(cageList.get(i));
                }else if(i < cageCount * 2 / 3){
                    arrangedCage[1].add(cageList.get(i));
                }else{
                    arrangedCage[2].add(cageList.get(i));
                }
            }
        }
    }

    private void reverse(ArrayList<Cage> arrangedCage){
        ArrayList<Cage> tmp = new ArrayList<>(arrangedCage);
        arrangedCage.clear();
        for(Cage cage : tmp){
            arrangedCage.add(0, cage);
        }
    }

    public void reArrange(){
        reverse(arrangedCage[0]);
        reverse(arrangedCage[1]);
        reverse(arrangedCage[2]);
        ArrayList<Cage> temp = arrangedCage[2];
        arrangedCage[2] = arrangedCage[1];
        arrangedCage[1] = arrangedCage[0];
        arrangedCage[0] = temp;
    }

    public String getType(){
        if (cageCount == 0){
            return "";
        }else{
            return cageList.get(0).getType();
        }
    }

    public int getCageCount(){
        return cageCount;
    }

    public String toString(){
        String str = "";
        for(int i = 2; i >= 0; i--){
            str += "level " + (i + 1) + ":";
            for(Cage cage : arrangedCage[i]){
                str += " " + cage + ",";
            }
            str += "\n";
        }
        return str;
    }

    public Animal getAnimal(String name){
        for(Cage cage : cageList){
            if(cage.getAnimal().getName().equals(name)){
                return cage.getAnimal();
            }
        }
        return null;
    }

}