package main;
import animal.*;
import cage.*;
import java.util.*;
public class JavariPark{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        Arrangement arrangement[] = new Arrangement[5];
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        AnimalFactory animalFactory = new AnimalFactory();
        String[] animalList = Animal.getAnimalList();
        for(int i = 0; i < animalList.length; i++){
            System.out.print(animalList[i] + ": ");
            int cnt = Integer.parseInt(scan.nextLine());
            if(cnt == 0)continue;
            System.out.println("Provide the information of " + animalList[i] + "(s):");
            String[] inp = scan.nextLine().split(",");
            
            ArrayList<Cage> cageList = new ArrayList<Cage>();
            for(String data : inp){
                String[] splitData = data.split("\\|");
                String name = splitData[0];
                int bodyLength = Integer.parseInt(splitData[1]);
                Animal animal = animalFactory.newObject(animalList[i], name, bodyLength);
                Cage cage = new Cage(animal);
                cageList.add(cage);
            }

            arrangement[i] = new Arrangement(cageList);
        }

        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");
        System.out.println("Cage arrangement:");

        for(int i = 0; i < animalList.length; i++){
            if(arrangement[i] == null) continue;
            System.out.println("location: " + arrangement[i].getType());
            arrangement[i].arrange();
            System.out.println(arrangement[i]);
            System.out.println("After rearrangement...");
            arrangement[i].reArrange();
            System.out.println(arrangement[i]);
        }

        System.out.println("NUMBER OF ANIMALS:");
        for(int i = 0; i < animalList.length; i++){
            System.out.println(animalList[i] + ":" + arrangement[i].getCageCount());
        }
        System.out.println("\n=============================================");


        while(true){
            animalList = new String[]{"Cat", "Eagle", "Hamster", "Parrot", "Lion"};
            String tmp = "Which animal you want to visit?\n(";
            for(int i = 0; i < animalList.length; i++){
                tmp += (i + 1) + ": " + animalList[i] + ", ";
            }
            tmp += "99: Exit)";
            System.out.println(tmp);
            int inp = Integer.parseInt(scan.nextLine());
            if(inp == 99)break;
            if(inp == 2)inp = 3;
            else if(inp == 3)inp = 5;
            else if(inp == 5)inp = 2;
            System.out.print("Mention the name of " + animalList[inp - 1] + " you want to visit: ");
            String name = scan.nextLine();
            Animal animal = arrangement[inp - 1].getAnimal(name);
            if(animal == null){
                System.out.println("There is no " + animalList[inp - 1] + " with that name! Back to the office!");
                System.out.println("Back to the office!\n");
                continue;
            }
            System.out.println("You are visiting " + name + " (" + animalList[inp - 1] + ") now, what would you like to do?");
            String[] commandList = animal.getCommands();
            for(int i = 0; i < commandList.length; i++){
                System.out.print((i + 1) + ": " + commandList[i] + " ");
            }
            System.out.println("");
            int command = Integer.parseInt(scan.nextLine());
            animal.getRespond(command - 1, scan);
            System.out.println("Back to the office!\n");
        }

    }
}