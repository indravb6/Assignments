package animal;
import java.util.*;
public class Cat extends Animal{
    
    private String commands[] = {"Brush the fur", "Cuddle"};

    public Cat(String name, int bodyLength){
        super(name, bodyLength);
    }

    public void getRespond(int command, Scanner scan){
        if(command == 0){
            System.out.println("Time to clean " + getName() + "'s fur");
            System.out.println(getName() + " makes a voice: Nyaaan...");
        }else if(command == 1){
            String[] respond = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
            Random rand = new Random();
            int n = rand.nextInt(respond.length);
            System.out.println(getName() + " makes a voice: " + respond[n]);
        }else{
            System.out.println("You do nothing...");
        }
    }

    public String[] getCommands(){
        return commands;
    }

    @Override
    public String getType(){
        return "Cat";
    }
}