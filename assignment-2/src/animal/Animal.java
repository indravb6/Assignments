package animal;
import java.util.*;
public abstract class Animal{
    private String name;
    private int bodyLength;
    private static final String[] animalList = {"cat", "lion", "eagle", "parrot", "hamster"};


    public Animal(String name, int bodyLength){
        this.name = name;
        this.bodyLength = bodyLength;
    }

    public String getName(){
        return name;
    }


    public int getBodyLength(){
        return bodyLength;
    }

    public static String[] getAnimalList(){
        return animalList;
    }

    public abstract void getRespond(int n, Scanner scan);
    public abstract String[] getCommands();
    public abstract String getType();

}