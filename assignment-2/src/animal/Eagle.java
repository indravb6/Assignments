package animal;
import java.util.*;
public class Eagle extends Animal{
    
    private String commands[] = {"Order to fly"};

    public Eagle(String name, int bodyLength){
        super(name, bodyLength);
    }

    public void getRespond(int command, Scanner scan){
        if(command == 0){
            System.out.println(getName() + " makes a voice: kwaakk...\nYou hurt!");
        }else{
            System.out.println("You do nothing...");
        }
    }

    public String[] getCommands(){
        return commands;
    }

    @Override
    public String getType(){
        return "Eagle";
    }
}