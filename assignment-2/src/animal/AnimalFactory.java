package animal;
public class AnimalFactory{
    public Animal newObject(String animalType, String name, int bodyLength){
        if(animalType.equals("cat")){
            return new Cat(name, bodyLength);
        }else if(animalType.equals("eagle")){
            return new Eagle(name, bodyLength);
        }else if(animalType.equals("hamster")){
            return new Hamster(name, bodyLength);
        }else if(animalType.equals("lion")){
            return new Lion(name, bodyLength);
        }else{
            return new Parrot(name, bodyLength);
        }
    }
}