package animal;
import java.util.*;
public class Lion extends Animal{
    
    private String[] commands = {"See it hunting", "Brush the mane", "Disturb it"};

    public Lion(String name, int bodyLength){
        super(name, bodyLength);
    }

    public void getRespond(int command, Scanner scan){
        if(command == 0){
            System.out.println("Lion is hunting..");
            System.out.println(getName() + " makes a voice: err...!");
        }else if(command == 1){
            System.out.println("Clean the lion's mane..");
            System.out.println(getName() + " makes a voice: Hauhhmm!");
        }else if(command == 2){
            System.out.println(getName() + " makes a voice: HAUHHMM!");
        }else{
            System.out.println("You do nothing...");
        }
    }

    @Override
    public String getType(){
        return "Lion";
    }
    public String[] getCommands(){
        return commands;
    }
}