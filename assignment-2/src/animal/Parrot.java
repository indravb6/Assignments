package animal;
import java.util.*;
public class Parrot extends Animal{
    
    private String commands[] = {"Order to fly", "Do conversation"};

    public Parrot(String name, int bodyLength){
        super(name, bodyLength);
    }

    public void getRespond(int command, Scanner scan){
        if(command == 0){
            System.out.println("Parrot " + getName() + " flies!");
            System.out.println(getName() + " makes a voice: FLYYYY...");
        }else if(command == 1){
            System.out.print("You say: ");
            String speech = scan.nextLine();
            System.out.println(getName() + " says: " + speech.toUpperCase());
        }else{
            System.out.println(getName() + " says: HM?");
        }
    }

    public String[] getCommands(){
        return commands;
    }

    @Override
    public String getType(){
        return "Parrot";
    }
}