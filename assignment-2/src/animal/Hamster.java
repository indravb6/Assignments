package animal;
import java.util.*;
public class Hamster extends Animal{
    
    private String commands[] = {"See it gnawing", "Order to run in the hamster wheel"};

    public Hamster(String name, int bodyLength){
        super(name, bodyLength);
    }

    public void getRespond(int command, Scanner scan){
        if(command == 0){
            System.out.println(getName() + " makes a voice: Ngkkrit.. Ngkkrrriiit");
        }else if(command == 1){
            System.out.println(getName() + " makes a voice: trrr... trrr...");
        }else{
            System.out.println("You do nothing...");
        }
    }

    public String[] getCommands(){
        return commands;
    }

    @Override
    public String getType(){
        return "Hamster";
    }
}