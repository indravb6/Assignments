import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Game {

  private final int SIZE = 6;
  private final String ICON_PATH = "assets/hero/";
  private final ImageIcon COVER = new ImageIcon("assets/logo.jpg");
  private final String TITLE = "Matching Game - Dota2";
  private Backend backend = new Backend(SIZE, ICON_PATH, COVER);

  public JPanel getButtonPanel() {
    JPanel panel = new JPanel();
    panel.setBackground(Color.black);
    panel.setLayout(new GridLayout(SIZE, SIZE));
    for (int i = 0; i < SIZE * SIZE; i++) {
      CardButton button = new CardButton(i, backend);
      backend.add(button);
      panel.add(button);
    }
    return panel;
  }

  public JPanel getMenuPanel() {
    JPanel panel = new JPanel();
    panel.setBackground(Color.black);
    panel.setLayout(new GridLayout(1, 3));
    InfoLabel infoLabel = new InfoLabel();
    TryAgainButton tryAgainButton = new TryAgainButton(backend);
    panel.add(infoLabel);
    panel.add(tryAgainButton);
    backend.add(infoLabel);
    return panel;
  }

  public void generateGUI(JFrame frame) {
    frame.setLayout(new BorderLayout());
    frame.add(getButtonPanel(), BorderLayout.CENTER);
    frame.add(getMenuPanel(), BorderLayout.SOUTH);
    frame.setSize(762, 500);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public void run() {
    generateGUI(new JFrame(TITLE));
  }

  public static void main(String args[]) {
    Game game = new Game();
    game.run();
  }

}