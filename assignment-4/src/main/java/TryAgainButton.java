import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

class TryAgainButton extends JButton {

  public TryAgainButton(Backend backend) {
    super("Try Again");
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        backend.tryAgain();
      }
    });
  }
}