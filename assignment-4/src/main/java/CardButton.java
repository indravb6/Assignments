import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

class CardButton extends JButton {
  private int id;

  public CardButton(int id, Backend backend) {
    setSize(127, 71);
    this.id = id;
    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        backend.click(id);
      }
    });
  }

  public int getId() {
    return id;
  }
}