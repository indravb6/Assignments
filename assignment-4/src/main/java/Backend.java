import java.util.ArrayList;
import java.util.Collections;
import java.io.File;

import javax.swing.ImageIcon;

public class Backend {

  private ArrayList<ImageIcon> iconList;
  private CardButton[] buttonList;
  private ImageIcon cover;
  private ArrayList<Integer> openButton;
  private String iconPath;
  private int size, tried;
  private InfoLabel info;

  public Backend(int size, String iconPath, ImageIcon cover) {
    this.iconPath = iconPath;
    this.cover = cover;
    this.size = size;
    this.buttonList = new CardButton[size * size];
    this.openButton = new ArrayList<>();
    loadFiles();
    makeDuplicateAndRandom();
  }

  public void loadFiles() {
    iconList = new ArrayList<>();
    try {
      File folder = new File(iconPath);
      File[] listOfFiles = folder.listFiles();
      for (File file : listOfFiles) {
        iconList.add(new ImageIcon(file.getAbsolutePath()));
      }
    } catch (Exception e) {
      System.out.println("Error: " + e.getMessage());
    }
  }

  public void makeDuplicateAndRandom() {
    Collections.shuffle(iconList);
    while (iconList.size() != size * size / 2) {
      iconList.remove(0);
    }
    for (int i = 0; i < size * size / 2; i++) {
      iconList.add(iconList.get(i));
    }
    Collections.shuffle(iconList);
  }

  private void open(int id) {
    buttonList[id].setIcon(iconList.get(id));
    openButton.add((Integer) id);
  }

  private void check() {
    if (openButton.size() >= 2 && iconList.get(openButton.get(0)) == iconList.get(openButton.get(1))) {
      buttonList[openButton.get(0)].setVisible(false);
      buttonList[openButton.get(1)].setVisible(false);
    } else {
      buttonList[openButton.get(0)].setIcon(cover);
      buttonList[openButton.get(1)].setIcon(cover);
    }
    openButton.remove(0);
    openButton.remove(0);
    tried++;
    updateInfo();
  }

  private void updateInfo() {
    info.setText("Number of tries: " + tried);
  }

  public void click(int id) {
    if (openButton.contains(id)) {
      return;
    }
    if (openButton.size() == 0) {
      open(id);
    } else if (openButton.size() == 1) {
      open(id);
    } else if (openButton.size() >= 2) {
      check();
      open(id);
    }
  }

  public void add(CardButton button) {
    buttonList[button.getId()] = button;
    button.setIcon(cover);
  }

  public void add(InfoLabel info) {
    tried = 0;
    this.info = info;
    updateInfo();
  }

  public void tryAgain() {
    this.openButton = new ArrayList<>();
    loadFiles();
    makeDuplicateAndRandom();
    tried = 0;
    updateInfo();
    for (CardButton button : buttonList) {
      button.setIcon(cover);
      button.setVisible(true);
    }
  }
}