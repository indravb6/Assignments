import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        TrainCar root = null;
        int n = Integer.parseInt(input.nextLine());

        while (n-- > 0) {
            String[] inputString = input.nextLine().split(",");
            WildCat wildCat = new WildCat(inputString[0], Double.parseDouble(inputString[1]), Double.parseDouble(inputString[2]));
            root = new TrainCar(wildCat, root);
            if (root.computeTotalWeight() > THRESHOLD || n == 0) {
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                root.printCar();
                System.out.printf("\nAverage mass index of all cats: %.2f\n", root.computeTotalMassIndex() / root.computeTotalCar());
                System.out.println("In average, the cats in the train are *" + root.getCategoryMassIndex() +"*");
                root = null;
            }
        }

    }
}
