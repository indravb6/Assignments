public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    } 

    public double computeTotalWeight() {
        return EMPTY_WEIGHT + cat.weight + (next != null ? next.computeTotalWeight() : 0.0);
    }

    public double computeTotalMassIndex() {
        return cat.computeMassIndex() + (next != null ? next.computeTotalMassIndex() : 0.0);
    }

    public int computeTotalCar() {
        return 1 + (next != null ? next.computeTotalCar() : 0);
    }

    public String getCategoryMassIndex(){
        double totalMassIndex = computeTotalMassIndex() / computeTotalCar();
        if (totalMassIndex < 18.5) return "underweight";
        else if (totalMassIndex < 25.0) return "normal";
        else if (totalMassIndex < 30.0) return "overweight";
        else return "obese";
    }

    public void printCar() {
        System.out.print("(" + cat.name + ")" + (next != null ? "--" : ""));
        if (next != null) next.printCar();
    }
}
