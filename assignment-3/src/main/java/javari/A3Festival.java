package javari;
import java.util.*;
import javari.reader.CsvReader;
import javari.repository.*;
import javari.writer.*;
import javari.park.*;
import java.nio.file.Paths;
import java.io.*;
import javari.attraction.*;
import javari.animal.abstraction.*;
public class A3Festival {

    public RegistrationWriter registrationWriter = new RegistrationWriter();
    public Scanner scan = new Scanner(System.in);
    public String[][] animalsAttractions;
    public String[][] animalsCategories;
    public String[][] animalsRecords;
    public CategoryRepository categoryRepository = new CategoryRepository();
    public AttractionRepository attractionRepository = new AttractionRepository();
    public AnimalRepository animalRepository = new AnimalRepository();
    public String type, attraction, name;

    // proses untuk membaca csv
    public void getInput(){
        animalsAttractions = new CsvReader("data/animals_attractions.csv").toArray();
        animalsCategories = new CsvReader("data/animals_categories.csv").toArray();
        animalsRecords = new CsvReader("data/animals_records.csv").toArray();
    }

    // memproses data categories
    public void prosesCategories(){
        int countSuccess = 0;
        for(String[] line : animalsCategories){
            boolean report = categoryRepository.registerCategory(line[0], line[1], line[2]);
            if(report){
                countSuccess++;
            }
        }
        System.out.println("Found _" + countSuccess + "_ valid sections and _" 
                            + (animalsCategories.length-countSuccess) + "_ invalid sections");
    }

    // memproses data attractions
    public void prosesAttractions(){
        int countSuccess = 0;
        for(String[] line : animalsAttractions){
            boolean report = attractionRepository.registerAttraction(line[0], line[1]);
            if(report){
                countSuccess++;
            }
        }
        System.out.println("Found _" + countSuccess + "_ valid attractions and _" 
                            + (animalsAttractions.length-countSuccess) + "_ invalid attractions");
    }

    // memproses data animals
    public void prosesAnimal(){
        int countSuccess = 0;
        for(String[] line : animalsRecords){
            boolean report = animalRepository.registerAnimal(line[0], line[1], line[2], line[3], 
                                                            line[4], line[5], line[6], line[7]);
            if(report){
                countSuccess++;
            }
        }
        System.out.println("Found _" + countSuccess + "_ valid animal records and _" 
                            + (animalsRecords.length-countSuccess) + "_ invalid animal records");
    }

    // mendapatkan input, category apa yg di inginkan visitor
    public void askCategory(){
        ArrayList<String> option = categoryRepository.getAvailableCategory();
        System.out.println("Javari Park has " + option.size() + " sections:");
        for(int i = 1; i <= option.size(); i++){
            System.out.println(i + ". " + option.get(i - 1));
        }
        System.out.print("Please choose your preferred section (type the number): ");
        int inp = Integer.parseInt(scan.nextLine());
        System.out.println();
        boolean callBack = askType(option.get(inp - 1));
        if(callBack){
            askCategory();
        }
    }

    // mendapatkan input, type apa yg di inginkan visitor
    public boolean askType(String category){
        ArrayList<String> option = categoryRepository.getAvailableType(category);
        System.out.println("--" + category + "--");
        for(int i = 1; i <= option.size(); i++){
            System.out.println(i + ". " + option.get(i - 1));
        }
        System.out.print("Please choose your preferred animals (type the number): ");
        String tmp = scan.nextLine();
        System.out.println();
        if(tmp.equals("#")){
            return true;
        }else{
            int inp = Integer.parseInt(tmp);
            type = option.get(inp - 1);
            boolean callBack = askAttraction(option.get(inp - 1));
            if(callBack){
                return askType(category);
            }
            return false;
        }
    }

    // mendapatkan input, attraction apa yg di inginkan visitor
    public boolean askAttraction(String type){
        ArrayList<Animal> dummyAnimal = animalRepository.getAnimalList(type);
        if(dummyAnimal.size() == 0 || dummyAnimal.get(0).isShowable() == false){
            System.out.println("\nUnfortunately, no " + type + " can perform any attraction, please choose other animals\n");
            return true;
        }
        ArrayList<String> option = attractionRepository.getAvailableAttraction(type);
        System.out.println("---" + type + "---");
        for(int i = 1; i <= option.size(); i++){
            System.out.println(i + ". " + option.get(i - 1));
        }
        System.out.print("Please choose your preferred attractions (type the number): ");
        String tmp = scan.nextLine();
        System.out.println();
        if(tmp.equals("#")){
            return true;
        }else{
            int inp = Integer.parseInt(tmp);
            attraction = option.get(inp - 1);
            askName();
            return false;
        }
    }

    // menanyakan nama visitor
    public void askName(){
        System.out.print("Wow, one more step,\nplease let us know your name: ");
        name = scan.nextLine();
    }

    // proses final, tulis output kedalam json
    public void sendReport(){
        System.out.println("\nYeay, final check!\nHere is your data, and the attraction you chose:");
        System.out.println("Name: " + name);
        System.out.println("Attractions: " + attraction + " -> " + type);
        System.out.println("With: " + animalRepository.getAnimalString(type));
        Attraction reportAttraction = new Attraction(attraction, type);
        ArrayList<Animal> animalList = animalRepository.getAnimalList(type);
        for(Animal animal : animalList){
            reportAttraction.addPerformer(animal);
        }
        Registration registration = new Registration(name, reportAttraction);
        try {
            registrationWriter.writeJson(registration, Paths.get(new File(".").getCanonicalPath()));
        } catch (Exception e){
            System.out.println(e);
        }
    }

    // validasi apakah program selesai atau ulang
    public void askAgain(){
        System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
        String tmp = scan.nextLine();
        if(tmp.equals("Y")){
            askCategory();
            sendReport();
            askAgain();
        }
    }

    public static void main(String args[]){
        A3Festival app = new A3Festival();
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("... Loading... Success... System is populating data...\n");
        app.getInput();
        app.prosesCategories();
        app.prosesAttractions();
        app.prosesAnimal();
        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        app.askCategory();
        app.sendReport();
        app.askAgain();
    }
}