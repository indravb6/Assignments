package javari.animal.category;
import javari.animal.abstraction.*;
public class Reptile extends Animal {

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, specialStatus, condition);
    }

    @Override
    protected boolean specificCondition(){
        // Only tame reptiles can do attractions
        return (getSpecialStatus().equals("tame") == true);
    }
}
