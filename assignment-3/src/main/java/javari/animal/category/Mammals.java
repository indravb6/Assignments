package javari.animal.category;
import javari.animal.abstraction.*;
public class Mammals extends Animal {

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, specialStatus, condition);
    }

    @Override
    protected boolean specificCondition(){
        // Mammals only can do attractions if they are not pregnant
        return (getSpecialStatus().equals("pregnant") == false);
    }
}
