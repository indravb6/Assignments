package javari.animal.category;
import javari.animal.abstraction.*;
public class Aves extends Animal {

    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, specialStatus, condition);
    }

    @Override
    protected boolean specificCondition(){
        // Aves only can do attractions if they are not laying eggs
        return (getSpecialStatus().equals("laying eggs") == false);
    }
}
