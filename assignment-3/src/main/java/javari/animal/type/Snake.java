package javari.animal.type;
import javari.animal.category.*;
import javari.animal.abstraction.*;
public class Snake extends Reptile {

    public Snake(Integer id, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, "Snake", name, gender, length, weight, specialStatus, condition);
    }
}
