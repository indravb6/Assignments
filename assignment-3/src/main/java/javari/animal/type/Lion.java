package javari.animal.type;
import javari.animal.category.*;
import javari.animal.abstraction.*;
public class Lion extends Mammals {

    public Lion(Integer id, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, "Lion", name, gender, length, weight, specialStatus, condition);
    }

    @Override
    protected boolean specificCondition(){
        // Only male lions can do attractions
        return (this.getGender() == Gender.MALE) && super.specificCondition();
    }
}
