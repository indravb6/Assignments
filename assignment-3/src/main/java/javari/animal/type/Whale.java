package javari.animal.type;
import javari.animal.category.*;
import javari.animal.abstraction.*;
public class Whale extends Mammals {

    public Whale(Integer id, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, "Whale", name, gender, length, weight, specialStatus, condition);
    }
}
