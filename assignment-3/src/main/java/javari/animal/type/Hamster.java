package javari.animal.type;
import javari.animal.category.*;
import javari.animal.abstraction.*;
public class Hamster extends Mammals {

    public Hamster(Integer id, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, "Hamster", name, gender, length, weight, specialStatus, condition);
    }
}
