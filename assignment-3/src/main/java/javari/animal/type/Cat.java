package javari.animal.type;
import javari.animal.category.*;
import javari.animal.abstraction.*;
public class Cat extends Mammals {

    public Cat(Integer id, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, "Cat", name, gender, length, weight, specialStatus, condition);
    }
}
