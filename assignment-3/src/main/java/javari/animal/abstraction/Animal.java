package javari.animal.abstraction;

public abstract class Animal {

    private Integer id;
    private String type;
    private String name;
    private Body body;
    private Condition condition;
    private String specialStatus;

    public Animal(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.body = new Body(length, weight, gender);
        this.specialStatus = specialStatus;
        this.condition = condition;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return body.getGender();
    }

    public double getLength() {
        return body.getLength();
    }

    public double getWeight() {
        return body.getWeight();
    }
    
    public Condition getCondition() {
        return condition;
    }

    public String getSpecialStatus() {
        return specialStatus;
    }
    
    public boolean isShowable() {
        return condition == Condition.HEALTHY && specificCondition();
    }
    
    protected abstract boolean specificCondition();
}
