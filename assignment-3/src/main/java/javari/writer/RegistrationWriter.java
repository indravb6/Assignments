package javari.writer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javari.animal.abstraction.Animal;
import javari.park.Registration;
import javari.attraction.SelectedAttraction;

import org.json.JSONWriter;

// Kelas untuk covert dari Registration ke JSON

public class RegistrationWriter {

    private static final String DEFAULT_FILENAME_FMT =
            "registration_%s.json";
    public static void writeJson(Registration registration,
                                 Path directory) throws IOException {
        String fileName = String.format(DEFAULT_FILENAME_FMT,
                registration.getVisitorName());
        String jsonFileName = fileName.replace(" ", "_");
        Path jsonFile = directory.resolve(jsonFileName);
        BufferedWriter fileWriter = Files.newBufferedWriter(jsonFile,
                StandardCharsets.UTF_8);
        final JSONWriter writer = new JSONWriter(fileWriter);
        buildRegistration(writer, registration);

        System.out.println("... End of program, write to "
                + jsonFile.getFileName().toString());
        fileWriter.flush();
        fileWriter.close();
    }
    private static void buildRegistration(JSONWriter writer,
                                          Registration registration) {
        writer.object()
                .key("registration_id").value(registration.getRegistrationId())
                .key("name").value(registration.getVisitorName())
                .key("attractions").object();
        
        buildAttraction(writer, registration.getSelectedAttractions());

        writer.endObject().endObject();;
    }
    private static void buildAttraction(JSONWriter writer,
                                        SelectedAttraction attraction) {
        String type = attraction.getType();
        writer.key("name").value(attraction.getName())
                .key("type").value(type);

        buildPerformers(writer,
                attraction.getPerformers());
    }
    private static void buildPerformers(JSONWriter writer,
                                        List<Animal> animals) {
        writer.key("animals").array();
        animals.forEach(animal -> buildPerformer(writer, animal));
        writer.endArray();
    }
    private static void buildPerformer(JSONWriter writer,
                                       Animal animal) {
        writer.object()
                .key("name").value(animal.getName())
                .endObject();
    }
}
