package javari.attraction;

import java.util.ArrayList;
import javari.animal.abstraction.Animal;

// data attraction
public class Attraction implements SelectedAttraction {
    
    private String name;
    private String type;
    private ArrayList<Animal> performers;

    public Attraction(String name, String type){
        this.name = name;
        this.type = type;
        this.performers = new ArrayList<Animal>();
    }

    public String getName(){
        return name;
    }

    public String getType(){
        return type;
    }

    public ArrayList<Animal> getPerformers(){
        return performers;
    }

    public boolean addPerformer(Animal performer){
        // @ TODO validasi
        performers.add(performer);
        return true;
    }
}
