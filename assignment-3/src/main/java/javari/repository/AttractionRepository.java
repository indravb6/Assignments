package javari.repository;
import java.util.*;

// Kelas untuk menyimpan attraction yang tersimpan pada program
public class AttractionRepository {

    private final String[] circlesOfFires = new String[] {"Lion", "Whale", "Eagle"}; 
    private final String[] dancingAnimals = new String[] {"Parrot", "Snake", "Cat", "Hamster"};
    private final String[] countingMasters = new String[] {"Hamster", "Whale", "Parrot"};
    private final String[] passionateCoders = new String[] {"Hamster", "Cat", "Snake"};

    private HashMap<String, ArrayList<String>> attractionList = new HashMap<String, ArrayList<String>>();

    // menambah attraction
    public boolean registerAttraction(String type, String attraction){
        if(validAttraction(type, attraction) == false){
            return false;
        }
        ArrayList<String> tmp = attractionList.get(type);
        if(tmp == null){
            tmp = new ArrayList<String>();
        }
        tmp.add(attraction);
        attractionList.put(type, tmp);
        return true;
    }

    // validasi attraction
    private boolean validAttraction(String type, String attraction){
        if(Arrays.asList(circlesOfFires).contains(type) && attraction.equals("Circles of Fires") 
            || Arrays.asList(dancingAnimals).contains(type) && attraction.equals("Dancing Animals")
            || Arrays.asList(countingMasters).contains(type) && attraction.equals("Counting Masters")
            || Arrays.asList(passionateCoders).contains(type) && attraction.equals("Passionate Coders")){
            return true;
        }else{
            return false;
        }
    }

    // mendapatkan attraction yang dapat dilakukan oleh type
    public ArrayList<String> getAvailableAttraction(String type){
        return attractionList.get(type);
    }
}