package javari.repository;
import java.util.*;

// Kelas untuk mengelola category yg tersimpan pada program
public class CategoryRepository {

    private final String[] mammalsList = new String[] {"Cat", "Hamster", "Lion", "Whale"};
    private final String[] avesList = new String[] {"Eagle", "Parrot"};
    private final String[] reptileList = new String[] {"Snake"};

    private ArrayList<String> mammalsRepository = new ArrayList<String>();
    private ArrayList<String> avesRepository = new ArrayList<String>();
    private ArrayList<String> reptileRepository = new ArrayList<String>();
    private ArrayList<String> allRepository = new ArrayList<String>();

    // menambah category baru
    public boolean registerCategory(String type, String category, String section){
        if(validCategory(type, category, section) == false){
            return false;
        }
        if(category.equals("mammals")){
            mammalsRepository.add(type);
        }else if(category.equals("aves")){
            avesRepository.add(type);
        }else{
            reptileRepository.add(type);
        }
        if(Arrays.asList(allRepository).contains(type) == false){
            allRepository.add(type);
        }
        return true;
    }

    // validasi category
    private boolean validCategory(String type, String category, String section){
        if(Arrays.asList(mammalsList).contains(type)){
            return (category.equals("mammals") && section.equals("Explore the Mammals"));
        }else if(Arrays.asList(avesList).contains(type)){
            return (category.equals("aves") && section.equals("World of Aves"));
        }else{
            return (category.equals("reptiles") && section.equals("Reptillian Kingdom"));
        }
    }

    // apakah type ada pada data
    public boolean validType(String type){
        return (Arrays.asList(allRepository).contains(type));
    }

    // mendapatkan daftar category
    public ArrayList<String> getAvailableCategory(){
        ArrayList<String> tmp = new ArrayList<String>();
        if(mammalsRepository.size() > 0){
            tmp.add("Explore the Mammals");
        }
        if(avesRepository.size() > 0){
            tmp.add("World of Aves");
        }
        if(reptileRepository.size() > 0){
            tmp.add("Reptillian Kingdom");
        }
        return tmp;
    }

    // mendapatkan daftar type yg dimiliki category
    public ArrayList<String> getAvailableType(String category){
        if(category.equals("Explore the Mammals")){
            return mammalsRepository;
        }else if(category.equals("World of Aves")){
            return avesRepository;
        }else{
            return reptileRepository;
        }
    }
}