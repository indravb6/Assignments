package javari.repository;
import java.util.*;
import javari.animal.type.*;
import javari.animal.abstraction.*;

// class untuk mengelola animal
public class AnimalRepository {

    private HashMap<String, ArrayList<Animal>> animalList = new HashMap<String, ArrayList<Animal>>();

    // mendaftarkan animal baru
    public boolean registerAnimal(String id, String type, String name, String gender, String length,
                  String weight, String specialStatus, String condition){
        Animal animal = createObject(Integer.parseInt(id), type, name, Gender.parseGender(gender), 
                                    Double.parseDouble(length), Double.parseDouble(weight), 
                                    specialStatus, Condition.parseCondition(condition));
        ArrayList<Animal> tmp = animalList.get(type);
        if(tmp == null){
            tmp = new ArrayList<Animal>();
        }
        tmp.add(animal);
        animalList.put(type, tmp);
        return true;
    }

    // factory
    public Animal createObject(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition){
        if(type.equals("Cat")){
            return new Cat(id, name, gender, length, weight, specialStatus, condition);
        }else if(type.equals("Eagle")){
            return new Eagle(id, name, gender, length, weight, specialStatus, condition);
        }else if(type.equals("Whale")){
            return new Whale(id, name, gender, length, weight, specialStatus, condition);
        }else if(type.equals("Hamster")){
            return new Hamster(id, name, gender, length, weight, specialStatus, condition);
        }else if(type.equals("Lion")){
            return new Lion(id, name, gender, length, weight, specialStatus, condition);
        }else if(type.equals("Parrot")){
            return new Parrot(id, name, gender, length, weight, specialStatus, condition);
        }else{
            return new Snake(id, name, gender, length, weight, specialStatus, condition);
        }
    }

    // dapatkan daftar animal dari type, output dalam string
    public String getAnimalString(String type){
        ArrayList<Animal> tmp = animalList.get(type);
        if(tmp == null){
            return "";
        }
        String ret = "";
        for(int i = 0; i < tmp.size(); i++){
            if(i > 0)ret += ", ";
            ret += tmp.get(i).getName();
        }
        return ret;
    }

    // dapatkan daftar animal dari type, output dalam arrayList
    public ArrayList<Animal> getAnimalList(String type){
        return animalList.get(type);
    }
}