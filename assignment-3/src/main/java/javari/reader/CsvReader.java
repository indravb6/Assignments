package javari.reader;

import java.io.*;
import java.util.*;

// fungsi untuk membaca Csv
// dibuat sangat basic karna format input tidak ada simbol aneh
public class CsvReader {
    
    private String path;

    public CsvReader(String path){
        this.path = path;
    }

    // mendapatkan array dua dimensi (of string) dari data
    public String[][] toArray(){
        String[][] result;
        ArrayList<String[]> tempResult = new ArrayList<String[]>();
        File file = new File(path);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null){
                tempResult.add(st.split(","));
            }
            result = new String[tempResult.size()][];
            result = tempResult.toArray(result);
        } catch (Exception e){
            System.out.println(e);
            result = new String[0][0];
        }
        return result;
    }
}
