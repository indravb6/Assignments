package javari.park;

import javari.attraction.SelectedAttraction;

// data pendaftar
public class Registration {
    
    private static int id = 1;
    private String visitorName;
    private SelectedAttraction attraction;

    public Registration(String visitorName, SelectedAttraction attraction){
        this.visitorName = visitorName;
        this.attraction = attraction;
    }

    public int getRegistrationId(){
        return id;
    }

    public String getVisitorName(){
        return visitorName;
    }


    public SelectedAttraction getSelectedAttractions(){
        return attraction;
    }
}
